---
output:
  html_document:
    code_folding: "hide"
pagetitle: Tractometry subconcussion - Descriptive Data
---

# Descriptives {.tabset .tabset-sticky .tabset-pills}

```{r source, message=FALSE, warning=FALSE, echo=FALSE}
source("0_helpers.R")
```

```{r load data, message=FALSE, warning=FALSE, echo=FALSE}
# Load data
df = readRDS("data/tractometry_subconcussion_long.rds")
df_wide <- read_csv("data/tractometry_subconcussion_wide.csv", col_names = TRUE) # for wide format analyses (jamovi and mixed anovas)

# Arrange factors
df_wide$Sex <- factor(df_wide$Sex,
                 levels = c("Female", "Male"),
                 labels = c("Female", "Male"))

# Label Group factor levels
df_wide$Group <- factor(df_wide$Group,
                   levels = c("CTRL_A", "CTRL_NA", "SUB"),
                   labels = c("CTRL_A", "CTRL_NA", "SUB"))
```
## Summary tables

Currently having some issues to display *all* the descriptive statistics in a nice dataframe. However, you can view the whole descriptive stats in this embedded [PDF document](website/descriptives.pdf).

**.CSV** files of the data can be download in [**long**](website/tractometry_subconcussion_long_2019-06-28.csv) or [**wide**](website/tractometry_subconcussion_wide_2019-05-30.csv) format ("Right click and save as" on [**long**](website/tractometry_subconcussion_long_2019-06-28.csv) or [**wide**](website/tractometry_subconcussion_wide_2019-05-30.csv)). If you don't know what the differences are between both format, this [**link**](https://www.theanalysisfactor.com/wide-and-long-data/) explains it well in addition to giving the pros and cons the form help you understand. Most GUI statistical software (e.g., Jamovi, SPSS...) need the data in a wide format.

<iframe src="https://samguay.gitlab.io/tractometry/website/descriptives.pdf" width=100% height="900x" allow="fullscreen"></iframe>
``` {r Summary, message=FALSE, warning=FALSE}
# Summary stats ----

summary_data <- summarySEwithin(df, measurevar="Mean", withinvars=c("Group","Tracts", "Measures"), idvar="Sub_id")
kable(summary_data, caption = "Overview of each group * tracts * measures") %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed", "responsive")) %>%
  row_spec(0, color = "white", background = "#2c3e50") %>%
  scroll_box(width = "100%", height = "700px")

```

## Corpus Callosum Tracts {.tabset .tabset-pills}

### Fractional Anisotropy (FA)
```{r CC Graph making, echo=FALSE}

# Filter the right tracts and measure = FA in Corpus Callosum

df_graph <- df %>% # always same name to save time
  filter(Measures == "fa")  %>%
  filter(Tracts != "CST_L_cut") %>%
  filter(Tracts != "CST_R_cut") %>%
  filter(Group != "NA") %>%
  select(Sub_id, Group, Tracts, Measures, Mean)
```
```{r, out.width = "100%", fig.align = "center"}
# The palette with grey:
cbPalette <- c("#999999", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")

# make graph
ggplot(df_graph, aes(x = Tracts, y = Mean, fill = Group)) +
  geom_flat_violin(aes(fill = Group),position = position_nudge(x = .15, y = 0), adjust = 1.5, trim = FALSE, alpha = .5, colour = NA)+
  geom_point(aes(x = as.numeric(Tracts)-.25, y = Mean, colour = Group), alpha =.5, position = position_jitter(width = .1), size = .75, shape = 20)+
  geom_boxplot(aes(x = Tracts, y = Mean, fill = Group),outlier.shape = NA, alpha = .5, width = .25, colour = "black")+
  ggtitle("Corpus Callosum Tracts x Groups on Mean FA")+
  ylab("Mean Fractional Anisotropy") + xlab("CC Tracts")+
  scale_fill_manual(name = "", labels = c("Control Athletes", "Control Non-Athletes", "Contact-Sport Athletes"), values=cbPalette)+
  scale_colour_manual(name = "", labels = c("Control Athletes", "Control Non-Athletes", "Contact-Sport Athletes"), values=cbPalette)+
  theme(legend.position = "bottom")
```
```{r save, message=FALSE, warning=FALSE, echo=FALSE}
# save graph
ggsave('output/fig/CC_tracts_unsegmented_FA.png', width = 10, height = 5)
```
<div class="alert alert-warning">
  <h2>Interactive graph with suspected outliers</h2>
  <p> You can zoom, remove one group by clicking on it in the legend, etc. </p>
</div>
```{r plotly, out.width = "100%", fig.height=9, fig.align = "center", message=FALSE, warning=FALSE}
plot_ly(df_graph, x = ~Tracts, y = ~Mean, color = ~Group, type = "box", boxpoints = 'outliers',
              marker = list(outlierwidth = 3),
              line = list(color = 'black')) %>%
  layout(boxmode = "group") %>%
  layout(legend = list(orientation = 'h', x = 0.225, y = -0.1))
```

### Mean Diffusivity (MD)
```{r MD, out.width = "100%", fig.align = "center"}
# Filter the right tracts and measure = MD in Corpus Callosum
df_graph <- df %>% # always same name to save time
  filter(Measures == "md")  %>%
  filter(Tracts != "CST_L_cut") %>%
  filter(Tracts != "CST_R_cut") %>%
  select(Sub_id, Group, Tracts, Measures, Mean)

# make graph
ggplot(df_graph, aes(x = Tracts, y = Mean, fill = Group)) +
  geom_flat_violin(aes(fill = Group),position = position_nudge(x = .15, y = 0), adjust = 1.5, trim = FALSE, alpha = .5, colour = NA)+
  geom_point(aes(x = as.numeric(Tracts)-.25, y = Mean, colour = Group), alpha =.5, position = position_jitter(width = .1), size = .75, shape = 20)+
  geom_boxplot(aes(x = Tracts, y = Mean, fill = Group),outlier.shape = NA, alpha = .5, width = .25, colour = "black")+
  ggtitle("Corpus Callosum Tracts x Groups on Mean MD")+
  ylab("Mean Mean Diffusity") + xlab("CC Tracts")+
  scale_fill_manual(name = "", labels = c("Control Athletes", "Control Non-Athletes", "Contact-Sport Athletes"), values=cbPalette)+
  scale_colour_manual(name = "", labels = c("Control Athletes", "Control Non-Athletes", "Contact-Sport Athletes"), values=cbPalette)+
  theme(legend.position = "bottom")
```
```{r message=FALSE, warning=FALSE, echo=FALSE}
# save graph
ggsave('output/fig/CC_tracts_unsegmented_MD.png', width = 10, height = 5)
########################################################################
```
<div class="alert alert-warning">
  <h2>Interactive graph with suspected outliers</h2>
  <p> You can zoom, remove one group by clicking on it in the legend, etc. </p>
</div>
```{r, out.width = "100%", fig.height=9, fig.align = "center", message=FALSE, warning=FALSE}
plot_ly(df_graph, x = ~Tracts, y = ~Mean, color = ~Group, type = "box", boxpoints = 'outliers',
              marker = list(outlierwidth = 3),
              line = list(color = 'black')) %>%
  layout(boxmode = "group") %>%
  layout(legend = list(orientation = 'h', x = 0.225, y = -0.15))
```

### Radial Diffusivity (RD)
```{r RD, out.width = "100%", fig.align = "center"}
# Filter the right tracts and measure = RD in Corpus Callosum
df_graph <- df %>% # always same name to save time
  filter(Measures == "rd")  %>%
  filter(Tracts != "CST_L_cut") %>%
  filter(Tracts != "CST_R_cut") %>%
  select(Sub_id, Group, Tracts, Measures, Mean)

# make graph
ggplot(df_graph, aes(x = Tracts, y = Mean, fill = Group)) +
  geom_flat_violin(aes(fill = Group),position = position_nudge(x = .15, y = 0), adjust = 1.5, trim = FALSE, alpha = .5, colour = NA)+
  geom_point(aes(x = as.numeric(Tracts)-.25, y = Mean, colour = Group), alpha =.5, position = position_jitter(width = .1), size = .75, shape = 20)+
  geom_boxplot(aes(x = Tracts, y = Mean, fill = Group),outlier.shape = NA, alpha = .5, width = .25, colour = "black")+
  ggtitle("Corpus Callosum Tracts x Groups on Mean RD")+
  ylab("Mean Radial Diffusity") + xlab("CC Tracts")+
  scale_fill_manual(name = "", labels = c("Control Athletes", "Control Non-Athletes", "Contact-Sport Athletes"), values=cbPalette)+
  scale_colour_manual(name = "", labels = c("Control Athletes", "Control Non-Athletes", "Contact-Sport Athletes"), values=cbPalette)+
  theme(legend.position = "bottom")
```
```{r message=FALSE, warning=FALSE, echo=FALSE}
# save graph
ggsave('output/fig/CC_tracts_unsegmented_RD.png', width = 10, height = 5)
########################################################################
```
<div class="alert alert-warning">
  <h2>Interactive graph with suspected outliers</h2>
  <p> You can zoom, remove one group by clicking on it in the legend, etc. </p>
</div>
```{r, out.width = "100%", fig.height=9, fig.align = "center", message=FALSE, warning=FALSE}
plot_ly(df_graph, x = ~Tracts, y = ~Mean, color = ~Group, type = "box", boxpoints = 'outliers',
              marker = list(outlierwidth = 3),
              line = list(color = 'black')) %>%
  layout(boxmode = "group") %>%
  layout(legend = list(orientation = 'h', x = 0.225, y = -0.15))
```

### Axial Diffusivity (AD)
```{r AD, out.width = "100%", fig.align = "center"}

# Filter the right tracts and measure = AD in Corpus Callosum
df_graph <- df %>% # always same name to save time
  filter(Measures == "ad")  %>%
  filter(Tracts != "CST_L_cut") %>%
  filter(Tracts != "CST_R_cut") %>%
  select(Sub_id, Group, Tracts, Measures, Mean)

# make graph
ggplot(df_graph, aes(x = Tracts, y = Mean, fill = Group)) +
  geom_flat_violin(aes(fill = Group),position = position_nudge(x = .15, y = 0), adjust = 1.5, trim = FALSE, alpha = .5, colour = NA)+
  geom_point(aes(x = as.numeric(Tracts)-.25, y = Mean, colour = Group), alpha =.5, position = position_jitter(width = .1), size = .75, shape = 20)+
  geom_boxplot(aes(x = Tracts, y = Mean, fill = Group),outlier.shape = NA, alpha = .5, width = .25, colour = "black")+
  ggtitle("Corpus Callosum Tracts x Groups on Mean AD")+
  ylab("Mean Axial Diffusity") + xlab("CC Tracts")+
  scale_fill_manual(name = "", labels = c("Control Athletes", "Control Non-Athletes", "Contact-Sport Athletes"), values=cbPalette)+
  scale_colour_manual(name = "", labels = c("Control Athletes", "Control Non-Athletes", "Contact-Sport Athletes"), values=cbPalette)+
  theme(legend.position = "bottom")
```
```{r message=FALSE, warning=FALSE, echo=FALSE}
# save graph
ggsave('output/fig/CC_tracts_unsegmented_AD.png', width = 10, height = 5)
```
<div class="alert alert-warning">
  <h2>Interactive graph with suspected outliers</h2>
  <p> You can zoom, remove one group by clicking on it in the legend, etc. </p>
</div>
```{r, out.width = "100%", fig.height=9, fig.align = "center", message=FALSE, warning=FALSE}
plot_ly(df_graph, x = ~Tracts, y = ~Mean, color = ~Group, type = "box", boxpoints = 'outliers',
              marker = list(outlierwidth = 3),
              line = list(color = 'black')) %>%
  layout(boxmode = "group") %>%
  layout(legend = list(orientation = 'h', x = 0.225, y = -0.1))
```

## Corticospinal Tracts {.tabset .tabset-pills}

### Fractional Anisotropy (FA)
```{r, out.width = "100%", fig.align = "center"}

# Filter the right tracts and measure = FA in CST
df_graph <- df %>%
  filter(Measures == "fa")  %>%
  filter(Tracts == "CST_R_cut" | Tracts == "CST_L_cut") %>%
  select(Sub_id, Group, Tracts, Measures, Mean)

# make graph
ggplot(df_graph, aes(x = Tracts, y = Mean, fill = Group)) +
  geom_flat_violin(aes(fill = Group),position = position_nudge(x = .15, y = 0), adjust = 1.5, trim = FALSE, alpha = .5, colour = NA)+
  # geom_point(aes(x = as.numeric(Tracts)-.15, y = Mean, colour = Group), alpha =.5, position = position_jitter(width = .1), size = .75, shape = 20)+ # can't make this one work
  geom_boxplot(aes(x = Tracts, y = Mean, fill = Group),outlier.shape = NA, alpha = .5, width = .25, colour = "black")+
  scale_colour_brewer(palette = "Dark2")+
  scale_fill_brewer(name = "Groups", labels = c("A", "B", "C"), palette = "Dark2")+
  ggtitle("Corticospinal Tracts x Groups on Mean FA")+
  ylab("Mean Fractional Anisotropy") + xlab("Corticospinal Tracts")+
  scale_x_discrete(labels = c('Left','Right'))+
  theme(legend.position = "bottom")
```
```{r message=FALSE, warning=FALSE, echo=FALSE}
# save graph
ggsave('output/fig/CST_tracts_unsegmented_FA.png', width = 10, height = 5)
```
<div class="alert alert-warning">
  <h2>Interactive graph with suspected outliers</h2>
  <p> You can zoom, remove one group by clicking on it in the legend, etc. </p>
</div>
```{r, out.width = "100%", fig.height=9, fig.align = "center", message=FALSE, warning=FALSE}
plot_ly(df_graph, x = ~Tracts, y = ~Mean, color = ~Group, type = "box", boxpoints = 'outliers',
              marker = list(outlierwidth = 3),
              line = list(color = 'black')) %>%
  layout(boxmode = "group") %>%
  layout(legend = list(orientation = 'h', x = 0.225, y = -0.15))
```

### Mean Diffusivity (MD)
```{r, out.width = "100%", fig.align = "center"}
# Filter the right tracts and measure = MD in CST
df_graph <- df %>%
  filter(Measures == "md")  %>%
  filter(Tracts == "CST_R_cut" | Tracts == "CST_L_cut") %>%
  select(Sub_id, Group, Tracts, Measures, Mean)

# make graph
ggplot(df_graph, aes(x = Tracts, y = Mean, fill = Group)) +
  geom_flat_violin(aes(fill = Group),position = position_nudge(x = .15, y = 0), adjust = 1.5, trim = FALSE, alpha = .5, colour = NA)+
  # geom_point(aes(x = as.numeric(Tracts)-.15, y = Mean, colour = Group), alpha =.5, position = position_jitter(width = .1), size = .75, shape = 20)+ # can't make this one work
  geom_boxplot(aes(x = Tracts, y = Mean, fill = Group),outlier.shape = NA, alpha = .5, width = .25, colour = "black")+
  scale_colour_brewer(palette = "Dark2")+
  scale_fill_brewer(name = "Groups", labels = c("A", "B", "C"), palette = "Dark2")+
  ggtitle("Corticospinal Tracts x Groups on Mean MD")+
  ylab("Mean Mean Diffusivity") + xlab("Corticospinal Tracts")+
  scale_x_discrete(labels = c('Left','Right'))+
  theme(legend.position = "bottom")

# save graph
ggsave('output/fig/CST_tracts_unsegmented_MD.png', width = 10, height = 5)
```
<div class="alert alert-warning">
  <h2>Interactive graph with suspected outliers</h2>
  <p> You can zoom, remove one group by clicking on it in the legend, etc. </p>
</div>
```{r, out.width = "100%", fig.height=9, fig.align = "center", message=FALSE, warning=FALSE}
plot_ly(df_graph, x = ~Tracts, y = ~Mean, color = ~Group, type = "box", boxpoints = 'outliers',
              marker = list(outlierwidth = 3),
              line = list(color = 'black')) %>%
  layout(boxmode = "group") %>%
  layout(legend = list(orientation = 'h', x = 0.225, y = -0.15))
```

### Radial Diffusivity (RD)
```{r, out.width = "100%", fig.align = "center"}
# Filter the right tracts and measure = RD in CST
df_graph <- df %>%
  filter(Measures == "rd")  %>%
  filter(Tracts == "CST_R_cut" | Tracts == "CST_L_cut") %>%
  select(Sub_id, Group, Tracts, Measures, Mean)

# make graph
ggplot(df_graph, aes(x = Tracts, y = Mean, fill = Group)) +
  geom_flat_violin(aes(fill = Group),position = position_nudge(x = .15, y = 0), adjust = 1.5, trim = FALSE, alpha = .5, colour = NA)+
  # geom_point(aes(x = as.numeric(Tracts)-.15, y = Mean, colour = Group), alpha =.5, position = position_jitter(width = .1), size = .75, shape = 20)+ # can't make this one work
  geom_boxplot(aes(x = Tracts, y = Mean, fill = Group),outlier.shape = NA, alpha = .5, width = .25, colour = "black")+
  scale_colour_brewer(palette = "Dark2")+
  scale_fill_brewer(name = "Groups", labels = c("A", "B", "C"), palette = "Dark2")+
  ggtitle("Corticospinal Tracts x Groups on Mean RD")+
  ylab("Mean Radial Diffusivity") + xlab("Corticospinal Tracts")+
  scale_x_discrete(labels = c('Left','Right'))+
  theme(legend.position = "bottom")
```
```{r message=FALSE, warning=FALSE, echo=FALSE}
# save graph
ggsave('output/fig/CST_tracts_unsegmented_RD.png', width = 10, height = 5)
```
<div class="alert alert-warning">
  <h2>Interactive graph with suspected outliers</h2>
  <p> You can zoom, remove one group by clicking on it in the legend, etc. </p>
</div>
```{r, out.width = "100%", fig.height=9, fig.align = "center", message=FALSE, warning=FALSE}
plot_ly(df_graph, x = ~Tracts, y = ~Mean, color = ~Group, type = "box", boxpoints = 'outliers',
              marker = list(outlierwidth = 3),
              line = list(color = 'black')) %>%
  layout(boxmode = "group") %>%
  layout(legend = list(orientation = 'h', x = 0.225, y = -0.15))
```

### Axial Diffusivity (AD)
```{r, out.width = "100%", fig.align = "center"}
# Filter the right tracts and measure = AD in CST
df_graph <- df %>%
  filter(Measures == "ad")  %>%
  filter(Tracts == "CST_R_cut" | Tracts == "CST_L_cut") %>%
  select(Sub_id, Group, Tracts, Measures, Mean)

# make graph
ggplot(df_graph, aes(x = Tracts, y = Mean, fill = Group)) +
  geom_flat_violin(aes(fill = Group),position = position_nudge(x = .15, y = 0), adjust = 1.5, trim = FALSE, alpha = .5, colour = NA)+
  # geom_point(aes(x = as.numeric(Tracts)-.15, y = Mean, colour = Group), alpha =.5, position = position_jitter(width = .1), size = .75, shape = 20)+ # can't make this one work
  geom_boxplot(aes(x = Tracts, y = Mean, fill = Group),outlier.shape = NA, alpha = .5, width = .25, colour = "black")+
  scale_colour_brewer(palette = "Dark2")+
  scale_fill_brewer(name = "Groups", labels = c("A", "B", "C"), palette = "Dark2")+
  ggtitle("Corticospinal Tracts x Groups on Mean AD")+
  ylab("Mean Axial Diffusivity") + xlab("Corticospinal Tracts")+
  scale_x_discrete(labels = c('Left','Right'))+
  theme(legend.position = "bottom")
```
```{r message=FALSE, warning=FALSE, echo=FALSE}
# save graph
ggsave('output/fig/CST_tracts_unsegmented_AD.png', width = 10, height = 5)
```
<div class="alert alert-warning">
  <h2>Interactive graph with suspected outliers</h2>
  <p> You can zoom, remove one group by clicking on it in the legend, etc. </p>
</div>
```{r, out.width = "100%", fig.height=9, fig.align = "center", message=FALSE, warning=FALSE}
plot_ly(df_graph, x = ~Tracts, y = ~Mean, color = ~Group, type = "box", boxpoints = 'outliers',
              marker = list(outlierwidth = 3),
              line = list(color = 'black')) %>%
  layout(boxmode = "group") %>%
  layout(legend = list(orientation = 'h', x = 0.225, y = -0.15))
```

